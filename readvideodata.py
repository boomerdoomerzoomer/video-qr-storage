#!/usr/bin/python3
import cv2
import os
from pyzbar import pyzbar
import binascii
from tqdm import tqdm
import zlib
import argparse
import hashlib

#https://towardsdatascience.com/building-a-barcode-qr-code-reader-using-python-360e22dfb6e5

def md5(*strings):
    key=hashlib.md5()
    for i in strings:
            key.update(i)
    this=key.hexdigest()
    del key
    return this

def read_barcodes(frame):
    barcodes = pyzbar.decode(frame)
    for barcode in barcodes:
        barcode_info = barcode.data
        return barcode_info

def count_frames(path, override=False):
	video = cv2.VideoCapture(path)
	total = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
	video.release()
	return total

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('fn', help='Filename')
    args = parser.parse_args()
    camera = cv2.VideoCapture(args.fn)
    ret = True
    tframes = count_frames(args.fn)
    print(tframes,'frames')
    frames = 0
    filedata=b''
    lastframe=b''
    pbar = tqdm(total=tframes)
    while ret:
        #print(frames)
        ret, frame = camera.read()
        if not ret:
            break
        data = read_barcodes(frame)
        if data:
            if frames==0 and data.startswith(b'QRFILE: '):
                filename = data[8:]
            elif frames==0:
                raise Exception('Invalid file')
            if frames>0 and not data.startswith(b'EOF') and data!=lastframe:
                filedata += data
                lastframe = data
        frames+=1
        pbar.update(1)
    pbar.close()
    camera.release()
    
    print('Frames:',frames)
    print('Checksum:',md5(filedata))
    filedata = binascii.a2b_hex(filedata)
    open(filename, 'wb').write(filedata)
    print(filename.decode(),'decoded')

