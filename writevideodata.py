#!/usr/bin/python3
import qrcode
from PIL import Image
import os
import argparse
import zlib
import binascii
from tqdm import tqdm
import cv2
import numpy
import hashlib

def md5(*strings):
    key=hashlib.md5()
    for i in strings:
            key.update(i)
    this=key.hexdigest()
    del key
    return this

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('fn', help='Filename')
    args = parser.parse_args()
    
    blocksize = 500
    framerate = 20
    filename = args.fn
    print('Encoding {} at {}fps in {} bytes blocks'.format(filename,framerate,blocksize))
    inputfile = open(filename, 'rb').read()
    data = binascii.b2a_hex(inputfile)
    print('Checksum:',md5(data))
    parts = []
    
    while len(data)>blocksize:
        parts.append(data[:blocksize])
        data = data[blocksize:]
    parts.append(data)

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('{}.mp4'.format(filename), fourcc, framerate, (1500,1500))
    
    print('Parts:',len(parts))
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_Q,
        box_size=10,
        border=4,
    )
    qr.add_data("QRFILE: {}".format(filename))
    qr.make()
    img = qr.make_image(fill_color="black", back_color="white")
    img = img.resize((1500,1500))
    out.write(cv2.cvtColor(numpy.array(img.convert("L")), cv2.COLOR_RGB2BGR))

    for i in tqdm(range(len(parts))):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_Q,
            box_size=10,
            border=4,
        )

        data = parts[i]
        qr.add_data(data)
        qr.make()

        img = qr.make_image(fill_color="black", back_color="white")
        img = img.resize((1500,1500))
        out.write(cv2.cvtColor(numpy.array(img.convert("L")), cv2.COLOR_RGB2BGR))

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_Q,
        box_size=10,
        border=4,
    )
    qr.add_data("EOF")
    qr.make()
    img = qr.make_image(fill_color="black", back_color="white")
    img = img.resize((1500,1500))
    out.write(cv2.cvtColor(numpy.array(img.convert("L")), cv2.COLOR_RGB2BGR))
    
    out.release()
    print('Done')
        
